package com.genaromendez.firebasechat.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.genaromendez.firebasechat.Entidades.Usuario;
import com.genaromendez.firebasechat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistroActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;

    private EditText txtNombre, txtCorreo, txtPass, txtPassMatch;
    private Button btnRegistro;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        txtNombre = (EditText) findViewById(R.id.idRegistroNombre);
        txtCorreo = (EditText) findViewById(R.id.idRegistroCorreo);
        txtPass = (EditText) findViewById(R.id.idRegistroPass);
        txtPassMatch = (EditText) findViewById(R.id.idRegistroPassMatch);

        btnRegistro = (Button) findViewById(R.id.idRegistroBoton);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String correo = txtCorreo.getText().toString();
                final String nombre = txtNombre.getText().toString();

                String pass = txtPass.getText().toString();

                if (isValidEmail(correo)  && validarPass() && validarNombre(nombre)){
                    pass = txtPass.getText().toString();

                    mAuth.createUserWithEmailAndPassword(correo, pass)
                            .addOnCompleteListener(RegistroActivity.this,new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if (task.isSuccessful()) {
                                        Toast.makeText(RegistroActivity.this, R.string.auth_success,
                                                Toast.LENGTH_SHORT).show();

                                        Usuario usuario = new Usuario();
                                        usuario.setCorreo(correo);
                                        usuario.setNombre(nombre);

                                        FirebaseUser currentUser = mAuth.getCurrentUser();
                                        DatabaseReference reference = database.getReference("Usuarios/"+currentUser.getUid());

                                        reference.setValue(usuario);

                                        Intent intent = new Intent(RegistroActivity.this,MainActivity.class);
                                        startActivity(intent);
                                        finish();

                                    } else {
                                        Toast.makeText(RegistroActivity.this, R.string.auth_failed,
                                                Toast.LENGTH_SHORT).show();

                                    }
                                }
                            });
                } else {
                    Toast.makeText(RegistroActivity.this, R.string.auth_failed2,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public final static boolean isValidEmail(CharSequence target){
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean validarPass(){
        String pass, passMatch;
        pass = txtPass.getText().toString();
        passMatch = txtPassMatch.getText().toString();

        if (pass.equals(passMatch)){
            if (pass.length()>=6 && pass.length()<=16){
                return true;
            }
        }

        return false;
    }

    public boolean validarNombre(String nombre){
        return !nombre.isEmpty();
    }
}
