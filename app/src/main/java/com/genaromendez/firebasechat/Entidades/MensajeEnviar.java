package com.genaromendez.firebasechat.Entidades;

import java.util.Map;

public class MensajeEnviar extends Mensaje {
    private Map hora;

    public MensajeEnviar() {

    }

    public MensajeEnviar(String type_mensaje, String nombre, String fotoPerfil, String mensaje, Map hora) {
        super(type_mensaje, nombre, fotoPerfil, mensaje);
        this.hora = hora;
    }

    public MensajeEnviar(String type_mensaje, String nombre, String fotoPerfil, String mensaje, String urlFoto, Map hora) {
        super(type_mensaje, nombre, fotoPerfil, mensaje, urlFoto);
        this.hora = hora;
    }

    public java.util.Map<String, String> getHora() {
        return hora;
    }

    public void setHora(java.util.Map<String, String> hora){
        this.hora = hora;
    }

}
