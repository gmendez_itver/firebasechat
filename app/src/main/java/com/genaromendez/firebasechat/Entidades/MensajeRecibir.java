package com.genaromendez.firebasechat.Entidades;

public class MensajeRecibir extends Mensaje {
    private Long hora;

    public MensajeRecibir(){

    }

    public MensajeRecibir(Long hora) {
        this.hora = hora;
    }

    public MensajeRecibir(String type_mensaje, String nombre, String fotoPerfil, String mensaje, String urlFoto, Long hora) {
        super(type_mensaje, nombre, fotoPerfil, mensaje, urlFoto);
        this.hora = hora;
    }

    public Long getHora() {
        return hora;
    }

    public void setHora(Long hora) {
        this.hora = hora;
    }
}
